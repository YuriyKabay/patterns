class Singleton {
  constructor() {
    if (this.constructor.instance) { return this.constructor.instance; }

    this.time = new Date();

    this.constructor.instance = this;
    return this.constructor.instance;
  }
}

const singleton = new Singleton();
console.log(singleton.time);

setTimeout(() => {
  const singleton2 = new Singleton();
  console.log(singleton2.time);
}, 4000);