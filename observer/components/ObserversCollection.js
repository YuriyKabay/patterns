class ObserversCollection {
  constructor () {
    this.observers = [];
  }

  add(object) {
    return this.observers.push(object);
  }

  getByIndex(index) {
    if (index !== -1 && this.observers.length > 0) {
      return this.observers[index];
    }
  }

  remove(object) {
    this.observers.splice(this.observers.findIndex(observer => observer === object), 1);
  }

  count() {
    return this.observers.length;
  }
}

module.exports = ObserversCollection;